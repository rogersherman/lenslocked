package controllers

import (
	"net/http"
	"github.com/simplycycling/lenslocked/views"
	"fmt"
)

// NewUsers is used to create a new Users controller. This function will
// panic if the templates are not parsed correctly, and should only be
// used during initial setup.
func NewUsers() *Users {
	return &Users{
		NewView: views.NewView("bootstrap", "users/new"),
	}
}

type Users struct {
	NewView *views.View
}

// This is used to render the form where a user can create a new
// user account.
// GET /signup
func (u *Users) New(w http.ResponseWriter, r *http.Request)  {
	u.NewView.Render(w, nil)
}

type SignupForm struct {
	Email		string `schema:"email"`
	Password	string `schema:"password"`
}

// This is used to process the signup form when a user tries to
// create a new user account.
//
// POST /signup
func (u *Users) Create(w http.ResponseWriter, r *http.Request)  {
	var form SignupForm
	if err := parseForm(r, &form); err != nil {
		panic(err)
	}
	fmt.Fprintln(w, form)
}